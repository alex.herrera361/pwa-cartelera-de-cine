import * as firebase from "firebase/app";
import "firebase/firestore";

// Agregar configuración firebase:
var firebaseConfig = {
    apiKey: "AIzaSyAjMoHtkyuLPcJxk96z8k5bhS9gq5mtPYw",
    authDomain: "first-pwa-5cf3d.firebaseapp.com",
    databaseURL: "https://first-pwa-5cf3d.firebaseio.com",
    projectId: "first-pwa-5cf3d",
    storageBucket: "first-pwa-5cf3d.appspot.com",
    messagingSenderId: "892642880936",
    appId: "1:892642880936:web:ff7b2fb10169011cea819d",
    measurementId: "G-4JNBWJNFVE"
  };

let firebaseApp = firebase.initializeApp(firebaseConfig);
let db = firebase.firestore();

export { db, firebase };
